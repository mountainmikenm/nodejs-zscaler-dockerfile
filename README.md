# Overview
This project contains example NodeJS `Dockerfile` templates which resolve build issues due to Zscaler SSL cetificate issues.  Templates include both pure JavaScript (non-building) and Typescript (building) examples.  

Your specific framework requirements may vary, but these templates serve to provide a starting point for a secure build.

### Problems Solved Using These Templates
Local Docker builds fail when the following conditions are met:
* Node dependencies include `@some-gitlab-group` scoped dependencies.
    ```json
      "dependencies": {
        "@some-gitlab-group/some-custom-package": "^0.0.1",
        "etc": "etc..."
      }
    ```
* `docker build` commands fail with SSL certificate errors such as:
    ```sh
    ------
    > [build 10/14] RUN npm install:
    #0 2.304 npm ERR! code UNABLE_TO_GET_ISSUER_CERT_LOCALLY
    #0 2.305 npm ERR! errno UNABLE_TO_GET_ISSUER_CERT_LOCALLY
    #0 2.312 npm ERR! request to https://gitlab.com/api/v4/packages/npm/@some-gitlab-group%2fsome-custom-package 
    #0 failed, reason: unable to get local issuer certificate
    ```
* Container Images contain secrets or other sensitive artifacts from the Docker build process.

### Cause
Zscaler is a cloud-based security solution.  It acts as a man-in-the-middle to inspect network traffic, but inserts an untrusted certificate chain signed by the **zScaler Issuing CA_**.

### Resolution
These Dockerfile templates download and install Zscaler root certificates during the build stage.  It then adds the certificates to the `.npmrc` Certificate Authority signing certificates.  This ensures successful connections between your local Docker build process and the GitLab package registry.  See lines ~32-55 in these examples.

### References
* https://docs.npmjs.com/cli/v9/using-npm/config#cafile
