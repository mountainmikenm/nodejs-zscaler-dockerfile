# Your project's base image. This is the image that will be used in both
# the build and runtime stages. Optionally pass in a different image
# using the --build-arg flag.
ARG NODE_BASE_IMAGE="node:99.99.99-alpine9.99"

#=====================================================================
# Build stage keeps build artifacts, secrets, etc separate.
# Layers from this stage may include sensitive information, so they
# should not be pushed to the registry.
#=====================================================================
FROM $NODE_BASE_IMAGE as build

# CI_JOB_TOKEN is a GitLab CI/CD variable that is used to authenticate
# with the GitLab package registry. It is passed in using the --build-arg
# flag. In local development, this variable is not set, so use a
# GitLab Personal Access Token ('PAT') instead. The PAT must have a minimum
# scope of 'read_api' and be exported to the shell or manully entered.
# See: https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
# Example:
# docker build -t someImageName --build-arg CI_JOB_TOKEN=$MY_PAT .
ARG CI_JOB_TOKEN

# Create the working directory for the build stage.
RUN mkdir -p /build
WORKDIR /build

# Add a root cert to address Zscaler issues
# Without this, we can't get to any of Alpine's package repos because Zscaler
# acts as a Man-in-the-middle and inserts an untrusted cert signed by the "zScaler Issuing CA"
RUN mkdir /usr/local/share/ca-certificates && \
    wget -cO /usr/local/share/ca-certificates/some-root-ca.crt http://some.host.com/some-cert.pem && \
    cat /usr/local/share/ca-certificates/some-root-ca.crt >> /etc/ssl/certs/ca-certificates.crt

# Adds common CA certs to the image.
RUN apk add --no-cache ca-certificates && update-ca-certificates

# Configure NPM to resolve private registry packages such as @some-gitlab-group/...
# scoped packages from the GitLab package registry.
# Then, set the auth token to CI_JOB_TOKEN or PAT passed in from build args.
# Finally, set the cafile to the root certs we added above.
# See: https://docs.gitlab.com/ee/user/packages/npm_registry/
# See: https://docs.npmjs.com/cli/v9/using-npm/config#cafile
RUN npm config set "@some-gitlab-group:registry" "https://gitlab.com/api/v4/packages/npm/" && \
    npm config set -- "//gitlab.com/api/v4/packages/npm/:_authToken" "${CI_JOB_TOKEN}" && \
    npm config set cafile /etc/ssl/certs/ca-certificates.crt

# Update NPM to the latest version supported by the Node base image.
RUN npm update -g npm@latest

# Install app dependencies
# Wildcard ensures both package.json AND package-lock.json are copied
COPY package*.json ./

# Install only the runtime dependencies, then clean up artifacts.
RUN npm ci --omit=dev && \
    npm prune && \
    npm cache clean --force && \
    rm -f /root/.npmrc && \
    rm -f .npmrc
#====================================================================
# End of build stage.
#====================================================================


#====================================================================
# Runtime stage to keep the image size small.
# Only layers from this stage will be pushed to the registry.
#====================================================================
FROM $NODE_BASE_IMAGE

# Define runtime defaults. Adjust these as needed.
ARG CONTAINER_PORT=3000
ENV NODE_ENV="production"

# Create runtime root directory.
WORKDIR /home/node

# Create a directory for the app logs.
RUN mkdir /var/log/app && \
    chown node /var/log/app

# Copy the app from the build stage. Only the files needed for runtime
# are copied over, so the image size is kept small.
COPY --from=build --chown=node:node /build/node_modules/ ./node_modules/
COPY --chown=node:node /config/ ./config/
COPY --chown=node:node /src/ ./src/

# Set the runtime user to 'node' for security. This user is created by the
# base image and does not have root privileges.
USER node

# Expose the port the app will listen on.
EXPOSE ${CONTAINER_PORT}

# Set the entrypoint script to run the app.
# Alternately, use CMD keyword to run the app directly.
ENTRYPOINT ["node", "./src/server.js"]
